﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrakenGUI.Models
{
    public class ArbitargeInfoElement
    {
        public string Pair { get; set; }
        public double ABvalue { get; set; }

        public double BAvalue { get; set; }

        public ArbitargeInfoElement(string pair, double abvalue, double bavalue)
        {
            Pair = pair;
            ABvalue = abvalue;
            BAvalue = bavalue;
        }

    }
}

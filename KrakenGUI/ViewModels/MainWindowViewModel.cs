﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KrakenApi;
using System.Windows;
using YahooFinanceData;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace KrakenGUI
{
    /// <summary>
    /// Viewmodel of the main Window. Offers some general data on the Assets on kraken.com
    /// </summary>
    internal class MainWindowViewModel:INotifyPropertyChanged
    {
        ///DataAccessOobject
        ///provided API to simply access REST api of kraken.com
        ///Offers no NotifyPropertychanged or other modern Helperfunctions...
        Kraken kraken;

        //statedata
        public Dictionary<string, AssetPair> assets;
        public Dictionary<string, AssetPair> marginEnabledPairs;

        //save list of Assets tradeable with margin
        public string marginEnabledPairsString;

        //filds of Data made aviable for GUI

        //List of Assets
        public List<string> assetsNames;

        //List of strings describing current Market
        public List<string> marginAssetMarketoverview;

        //List of ArbitargeInfoElements 
        public List<Models.ArbitargeInfoElement> arbitrageOpportunities;



        /// <summary>
        /// standard constructor, initiates the aquiering of the list of Assets aviable at kraken in backgroundthread
        /// </summary>
        public MainWindowViewModel()
        {
            //create DataAccessObject
            this.Kraken = new Kraken("Dummy", "Dummy");

            var task=Task.Factory.StartNew(() => {
                assets = this.Kraken.GetAssetPairs();

                //Save data on the more important margintradable Assets
                marginEnabledPairs = assets.Where(ele => ele.Value.LeverageBuy.Length != 0).ToDictionary(t => t.Key, t => t.Value);
                marginEnabledPairsString = marginEnabledPairs.Aggregate("", (a, b) => a + (a == "" ? "" : ",") + b.Key);

                UpdateAssetsNamesCollection();
                UpdateMarginAssetMarketoverview();
                UpdateArbitageInfo();
            });
            //Update List of all Tradeable Assetpairs
            
        }


        /// <summary>
        /// Our DataAccessObject
        /// </summary>
        public Kraken Kraken
        {
            get
            {
                return kraken;
            }
            private set
            {
                kraken = value;
            }
        }

        #region AssetsList

        /// <summary>
        /// Build new List of names of tradeable Assets
        /// </summary>
        internal void UpdateAssetsNamesCollection()
        {
            assetsNames=new List<string>();
            foreach (var asset in assets)
            {
                assetsNames.Add(asset.Value.Altname);
            }
            Assets = assetsNames;
        }

        /// <summary>
        /// Access the List of know Assets
        /// </summary>
        public List<string> Assets
        {
            get
            {
                return assetsNames;
            }
            set
            {
                assetsNames = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region MarginAssetOverview

        /// <summary>
        /// request current prices of assets that can be traded with margin.
        /// </summary>
        internal void UpdateMarginAssetMarketoverview()
        {
            var prices = kraken.GetTicker(marginEnabledPairsString);

            
            MarginAssetMarketoverview= (from asset in marginEnabledPairs.OrderByDescending(ele => ele.Value.LeverageBuy.Length)
                                        select asset.Value.Altname
                                                + " "
                                                + (asset.Value.LeverageBuy.Length > 0 ? asset.Value.LeverageBuy.Select(x => x.ToString()).Aggregate((a, b) => a + " " + b) : "")
                                                + " "
                                                + prices[asset.Key].Bid[0]).ToList();


        }

        /// <summary>
        /// simple overview over Market of Assets tradeable with margin
        /// </summary>
        public List<string> MarginAssetMarketoverview
        {
            get
            {
                return marginAssetMarketoverview;
            }
            set
            {
                marginAssetMarketoverview = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        #region arbitrageOpportunities

        /// <summary>
        /// Current arbitargeoppurtunities
        /// </summary>
        public List<Models.ArbitargeInfoElement> ArbitageOportunities
        {
            get
            {
                return arbitrageOpportunities;
            }
            set
            {
                arbitrageOpportunities = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// request current rate of USD/EUR from Yahoo and calulate implied USD/EUR rate on Kraken for different assets
        /// </summary>
        internal void UpdateArbitageInfo()
        {
            var task = Task.Factory.StartNew(() =>
            {
                List<Models.ArbitargeInfoElement> Data = new List<Models.ArbitargeInfoElement>();

                var tickers = kraken.GetTicker(AssetPairsIdentifier.BTCEUR + "," + AssetPairsIdentifier.BTCUSD + "," + AssetPairsIdentifier.ETHEUR + "," + AssetPairsIdentifier.ETHUSD + "," + AssetPairsIdentifier.ETCEUR + "," + AssetPairsIdentifier.ETCUSD);


                double forexPrice = EasyTicker.GetTicker(EasyTicker.EURUSD); //TODO run parellel to GetTicker....

                Data.Add(CalcArbOp(tickers, forexPrice, AssetPairsIdentifier.BTCUSD, AssetPairsIdentifier.BTCEUR));
                Data.Add(CalcArbOp(tickers, forexPrice, AssetPairsIdentifier.ETHUSD, AssetPairsIdentifier.ETHEUR));
                Data.Add(CalcArbOp(tickers, forexPrice, AssetPairsIdentifier.ETCUSD, AssetPairsIdentifier.ETCEUR));

                //Data.Add("EUR/USD: " + forexPrice.ToString());

                ArbitageOportunities = Data;
            });

        }

        /// <summary>
        /// Helperfunction that calculates implied exchangerate for specific assets and returns arbitargeInfoElement.
        /// </summary>
        /// <param name="tickers">Current Prices of Assets</param>
        /// <param name="forexPrice">Current real USD/EUR rate</param>
        /// <param name="firstPair">First Assets used in Hedge</param>
        /// <param name="secondPair">Second Asset used in Hedge</param>
        /// <returns>ArbitageInfoElement</returns>
        internal Models.ArbitargeInfoElement CalcArbOp(Dictionary<string, Ticker> tickers, double forexPrice, string firstPair, string secondPair)
        {

            //Mit dollar kaufen
            //Mit euro verkaufen
            //grün = AB kleiner forexprice -> euro besonders wenig kaufkraft-> mehr als normal euro pro btc -> Vorteilhaft btc zu leihen und zu verkaufen für euro. Mit USD kaufen um zu hedgen
            double AB = (double)(tickers[firstPair].Ask[0] / tickers[secondPair].Bid[0]);

            //Mit dollar verkaufen
            //Mit euro kaufen
            //grün = BA größer forexprice -> euro beonders viel kaufkraft -> weniger als normal euro pro btc -> Vorteilhaft euro zu leihen und btc zu kaufen. Mit USD hedgen.
            double BA = (double)(tickers[firstPair].Bid[0] / tickers[secondPair].Ask[0]);

            //Below forex
            return new Models.ArbitargeInfoElement(firstPair + "/" + secondPair, AB, BA);
        }

        #endregion

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

    }
}

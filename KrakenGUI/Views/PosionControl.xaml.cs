﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KrakenApi;

namespace KrakenGUI
{
    /// <summary>
    /// Interaction logic for TestUserControl.xaml
    /// </summary>
    public partial class PosionControl : UserControl
    {
        private MainWindow mainWindow;
        private IGrouping<string, KeyValuePair<string, PositionInfo>> pair;

        private decimal PositionSize;

        public PosionControl()
        {
            InitializeComponent();
        }

        public PosionControl(IGrouping<string, KeyValuePair<string, PositionInfo>> pair, MainWindow mainWindow) :this()
        {
            this.pair = pair;
            this.mainWindow = mainWindow;

            PositionSize = new decimal(0);
            decimal PL = new decimal(0);
            bool isShort = pair.First().Value.Type == "sell" ;


            foreach (var ele in pair)
            {
                PositionSize += ele.Value.Vol;
                PositionSize -= ele.Value.VolClosed;
                PL += ele.Value.Net;
            }

            //PositionMainStackPanel.Children.Add(new Label { Content = Pairs[pair.Key].Altname + " " + sum + " " + PL, Foreground = pair.First().Value.Type == "buy" ? Brushes.DarkGreen : Brushes.Red });
            this.PairName.Content = mainWindow.Pairs[pair.Key].Altname;
            this.Positionsize.Content = (isShort?"- ":"") + PositionSize.ToString();
            this.Positionsize.Foreground = isShort ? Brushes.Red : Brushes.Green;

            this.PL.Content = PL.ToString();
            this.PL.Foreground = PL > 0 ? Brushes.Green : Brushes.Red;
            this.CloseButton.Click += CloseButton_Click;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Close "+PositionSize+" of "+pair.Key);
            var Order=new KrakenOrder();
            Order.Leverage = 2;
            Order.Pair = pair.Key;
            Order.Type = pair.First().Value.Type == "buy" ? "sell" : "buy";
            Order.Volume = PositionSize;
            Order.OrderType = "market";

            var res=mainWindow.kraken.AddOrder(Order);
            MessageBox.Show(res.Descr.Order);
        }
    }
}

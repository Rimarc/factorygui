﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace YahooFinanceData
{
    public class EasyTicker
    {
        public const string EURUSD = "EURUSD";
        public const string USDCNH = "USDCNH";

        public static double GetTicker(string TickerName)
        {
            WebClient client = new WebClient();
            string reply = client.DownloadString("http://finance.yahoo.com/d/quotes.csv?e=.csv&f=l1&s="+TickerName+"=X");
            return double.Parse(reply.Split(',')[0], System.Globalization.CultureInfo.InvariantCulture);
        }
    }
}

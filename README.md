# KrakenApi
C# Library to access the Kraken REST API (https://www.kraken.com/) (This is not written by me, just some bugs fixed)

# KrakenGUI

Here is the part written by me, now using some simple MVVM. Its nonblocking, and easy extendable.

## License

See the [LICENSE](LICENSE) file for license rights and limitations (MIT).